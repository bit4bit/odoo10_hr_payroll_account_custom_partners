# -*- coding: utf-8 -*-
{
    'name': "hr_payroll_account_custom_partners",

    'summary': """
        Adiciona Terceros a empleado y se asignan en los
        asientos contables de la nómina generada.
     """,

    'description': """
    Relaciona Terceros con Empleados según reglas indicadas al generar la nómina.

    Este módulo requiere las Reglas de salariales previamente creadas,
    una vez creadas ingresar a "Nómina/Reglas de Terceros" y crear los registros
    para la asignación de terceros, al diligenciar el registro ingresar en *Código de regla salarial*
    el código de la regla salaria a la cuál se debe aplicar el Tercero al momento de generar la nómina.
    """,

    'author': "Ceiba - Cier",
    'website': "https://efossils.somxslibres.net/fossil/user/bit4bit/repository/odoo10_addons",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Human Resources',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'hr_payroll', 'hr_payroll_account'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'license': 'AGPL-3',
}
